import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'untitled7';

  @ViewChild('mycanvas') myCanvas: ElementRef;

  private ctx: CanvasRenderingContext2D;

  private xTotal: number;


  private yTotal: number;
  private xDots = 500; // 100
  private yDots = 500; // 100

  private static rotateX(x, y, z, gamma): Array<number> {

    const xRotX = x;
    const yRotX = y * Math.cos(gamma) + z * Math.sin(gamma);
    const zRotX = y * -1 * Math.sin(gamma) + z * Math.cos(gamma);

    return [xRotX, yRotX, zRotX];
  }

  private static rotateZ(x, y, z, gamma): Array<number> {

    x = x * Math.cos(gamma) + y * Math.sin(gamma);
    y = x * -1 * Math.sin(gamma) + y * Math.cos(gamma);

    return [x, y, z];
  }

  private static rotateY(x, y, z, gamma): Array<number> {

    x = x * Math.cos(gamma) + z * -1 * Math.sin(gamma);
    z = x * Math.sin(gamma) + z * Math.cos(gamma);

    return [x, y, z];
  }

  private static calcSurfacePointDonut(alpha: number, beta: number, gamma?: number, theta?: number): Array<number> {
    // const k1 = 50;
    // const k2 = 150;
    const r2 = 100;
    const r1 = 40;

    const x = (r2 + r1 * Math.cos(alpha)) * Math.cos(beta);
    const y = r1 * Math.sin(alpha);
    const z = (r2 + r1 * Math.cos(alpha)) * Math.sin(beta);

    // const xRotX = x;
    // const yRotX = y * Math.cos(gamma) + z * Math.sin(gamma);
    // const zRotX = y * -1 * Math.sin(gamma) + z * Math.cos(gamma);

    // const xHyphen = x * k1 / (k2 + z);
    // const yHyphen = y * k1 / (k2 + z);

    // const xHyphen = xRotX * k1 / (k2 + zRotX);
    // const yHyphen = yRotX * k1 / (k2 + zRotX);

    return [x, y, z];
    // return [1, 1];
  }

  private static projectInto2D(x, y, z): Array<number> {
    const k1 = 50;
    const k2 = 170; // 150

    const xHyphen = x * k1 / (k2 + z);
    const yHyphen = y * k1 / (k2 + z);

    return [xHyphen, yHyphen];
  }

  ngOnInit(): void {
  }

  async ngAfterViewInit(): Promise<any> {
    this.ctx = this.myCanvas.nativeElement.getContext('2d');

    this.xTotal = this.myCanvas.nativeElement.width;
    this.yTotal = this.myCanvas.nativeElement.height;


    const dotWidth = this.xTotal / this.xDots;
    const dotHeight = this.yTotal / this.yDots;


    const stepSize = 0.1;


    const surface = [];
    for (let currAlpha = 0; currAlpha <= 2 * Math.PI; currAlpha += stepSize) {
      for (let currBeta = 0; currBeta <= 2 * Math.PI; currBeta += stepSize) {

        // while (true) {


        // for (let currTheta = 0; currTheta <= 2 * Math.PI; currTheta += stepSize) {
        const surfPoint = AppComponent.calcSurfacePointDonut(currAlpha, currBeta);
        surface.push(surfPoint);

      }
      // }

      // await this.delay(30);

      // }

    }
    console.log('hello3');


    let currGamma = 0;
    // let currTheta = 0;
    // for (let currGamma = 0; currGamma <= 2 * Math.PI; currGamma += stepSize) {
    while (true) {

      currGamma = currGamma % (2 * Math.PI);

      let x;
      let y;
      let z;

      surface.forEach(surfPoint => {


        [x, y, z] = AppComponent.rotateX(surfPoint[0], surfPoint[1], surfPoint[2], currGamma);

        [x, y, z] = AppComponent.rotateY(x, y, z, currGamma);

        // [x, y, z] = this.rotateZ(x, y, z, currGamma);

        [x, y, z] = AppComponent.projectInto2D(x, y, z);

        [x, y, z] = this.shiftIntoView(x, y, z);


        this.drawDot(x, y, dotWidth, dotHeight);

      });

      await this.delay(40);
      this.clearCanvas();

      currGamma += stepSize;
      // currTheta += stepSize;
    }


  }

  private drawDot(x: number, y: number, dotWidth: number, dotHeight: number): void {
    this.ctx.fillStyle = '#000000';
    this.ctx.fillRect(x, y, dotWidth, dotHeight);
  }

  private clearCanvas(): void {
    this.ctx.clearRect(0, 0, this.xTotal, this.yTotal);
  }

  public async delay(ms: number): Promise<any> {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  private shiftIntoView(x: any, y: any, z: any): Array<number> {
    return [x + (this.xTotal / 2), y + (this.yTotal / 2)];
  }
}
